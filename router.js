class Router {
    // Private field is still experimental stage 3 and should not be used in production
    // Proposal https://github.com/tc39/proposal-class-fields
    // Supported in V8 7.4 https://v8.dev/blog/v8-release-74#javascript-language-features
    // Nodejs updated to V8 7.4 in v12 @PR https://github.com/nodejs/node/pull/26685
    // Nodejs 12 changelog https://nodejs.org/en/blog/release/v12.0.0/
    // Right now for private we can just move private fields out side the class and only export the class
    // So it will be just accessible for the class
    #path = [
    ]

    get(path, middleware) {
        this.#path.push({
            path,
            method: 'get',
            middleware
        })
        return this
    }

    post(path, middleware) {
        this.#path.push({
            path,
            method: 'post',
            middleware
        })
        return this
    }

    use(middleware) {
        this.#path.push({
            path: '',
            middleware
        })
        return this
    }

    #next = function (error) {
        this.nextRoute = true
        this.error = error
    }

    async process(req, res) {
        const status = {
            nextRoute: true,
            error: undefined
        }
        const next = this.#next.bind(status)
        const url = req.url
        const routes = this.#path.filter(route => 
            (url.indexOf(route.path) === 0 && route.method === req.method.toLowerCase()) || route.path === ''
        )
        for(let i = 0; i < routes.length; i++) {
            status.nextRoute = false
            await routes[i].middleware(req, res, next)
            if (!status.nextRoute) break
            if (status.error) {
                console.error(`\x1b[1;31mError:\x1b[0m ${status.error}`)
                break
            }
        }
    }
}

const router = new Router()
    .get('/simple', (req, res, next) => {
        console.log('First')
        next()
    }).use((req, res, next) => console.log('Second') || next('Play with error'))
router.process({url: '/simple', method: 'get'}, {})
